# elbe-RPi

Build Raspberry Pi image based on Debian using ELBE

## Installation

To install Elbe, refer to : https://elbe-rfs.org/docs/sphinx/article-quickstart.html

## Generation

For the first usage, you must generate the Virtual Machine : 

```elbe initvm create```

Then, you build the image :

```elbe initvm submit rpi3-arm64.xml```


For the other build, you can specify several parameters to speed you build time : 

```elbe initvm submit rpi3-arm64.xml  --skip-build-bin  --skip-build-sources --cdrom bin-cdrom.iso```